## SROP

### Overview
When kernel mode is entered from user mode, the current processor context is saved to
the user-land stack, and when user mode is entered from kernel mode, the processor context
is restored from the user-land stack. Specifically the low level sigreturn function sets
the processor context for the kernel->user-land transition.

![sigreturn](https://www.lazenca.net/download/attachments/16810278/1527519944764.jpg)

The exploit is to get control of [er]ip, set up our own frame struct on the stack describing 
the state we want, and then call sigreturn to set the processor into that state for us.

### Pros
- Full (pseudo kernel level) control over processor (can even set eflags)
- In the best case (32bit), defeats ASLR, PIE, and NX

### Cons
- Large payload (frame alone is 80 bytes for 32bit / 248 bytes for 64bit)
- Requires libld.so to be included (32bit), or specific gadgets (64bit)
- In the worst case (64bit), only defeats ASLR and NX (with a libc leak)

### Requirements

x86_32 (can be ASLR, PIE, NX)
- libc leak
- `libdl.so` (`gcc {...} -ldl`)

x86_64 (Can be ASLR, NX)
- libc leak 
- `mov rax, 0x0f` gadget
- `syscall` gadget

In the struct, certain registers describe the kernel/user context of the processor state.
The following must be set.
```
"i386" :         {"cs": 0x73, "ss": 0x7b},
"i386_on_amd64": {"cs": 0x23, "ss": 0x2b},
"amd64":         {"csgsfs": 0x33},
"arm":           {"trap_no": 0x6, "cpsr": 0x40000010, "VFPU-magic": 0x56465001, "VFPU-size": 0x120},
"mips":          {},
"aarch64":       {"magic": 0x0000021046508001},
```
### Diagram of this exploit (x86_32)
```
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                                                               |
+                                                               +
|                                                               |
+                            padding                            +
|                                                               |
+                                                               +
|                                                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                           sigreturn                           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              junk                             |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                               gs                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                               fs                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                               es                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                               ds                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              edi                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              esi                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              ebp                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              esp                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                         ebx = /bin/sh                         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              edx                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              ecx                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                           eax = 0x0b                          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                             trapno                            |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                              err                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                         eip = int 0x80                        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                           cs = 0x23                           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                             eflags                            |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                          esp_atsignal                         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                           ss = 0x2b                           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                            fp_state                           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```

### To generate this diagram

`protocol "padding:136,sigreturn:32,junk:32,gs:32,fs:32,es:32,ds:32,edi:32,esi:32,ebp:32,esp:32,ebx:32,edx:32,ecx:32,eax:32,trapno:32,err:32,eip:32,cs:32,eflags:32,esp_atsignal:32,ss:32,fp_state:32"`