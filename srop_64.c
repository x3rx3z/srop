#include <stdlib.h>
#include <stdio.h>

/* gcc -o srop -fno-stack-protector srop.c -ldl */

void usefulGadgets() {
	__asm__("movl $0x0f, %eax; ret;");
	__asm__("syscall; ret;");
}

void vuln() {
	char buff[128];
	printf("printf: %p\n", (void *)printf );
	setbuf(stdout, NULL);
	gets(buff);
}

int main( char *argc, char **argv ) {
	vuln();
	return 0;
}