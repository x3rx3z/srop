#include <stdlib.h>
#include <stdio.h>

/* gcc -o srop -fno-stack-protector -m32 srop.c -ldl */

void vuln() {
	char buff[128];
	setbuf(stdout, NULL);
	gets(buff);
}

int main( char *argc, char **argv ) {
	printf("printf: %p\n", (void *)printf );
	vuln();
	return 0;
}